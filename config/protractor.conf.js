require('ts-node/register');
var helpers = require('./helpers');

exports.config = {
  baseUrl: 'http://localhost:3000/',
  specs: [
    helpers.root('src/**/**.e2e.ts'),
    helpers.root('src/**/*.e2e.ts')
  ],
  exclude: [],
  framework: 'jasmine2',
  allScriptsTimeout: 110000,
  jasmineNodeOpts: {
    showColors: true,
    silent: true,
    isVerbose: false,
    includeStackTrace: false,
    print: function () {
    },
    realtimeFailure: false
  },
  directConnect: true,
  chromeOnly: true,
  capabilities: {
    'browserName': 'chrome'
  },
  onPrepare: function() {
    browser.ignoreSynchronization = true;
    var SpecReporter = require('jasmine-spec-reporter');
    jasmine.getEnv().addReporter(new SpecReporter({
      displayFailuresSummary: false
    }));
  },
  useAllAngular2AppRoots: true
};

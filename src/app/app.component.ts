import { Component, ViewEncapsulation, ViewContainerRef, OnInit } from '@angular/core';
import { RequestOptions } from '@angular/http';

/*
 * App Component
 * Top Level Component
 */
@Component({
  selector: 'app',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './app.component.jade'
})
export class AppComponent {

  constructor(private viewContainerRef: ViewContainerRef) {
  }
}

import { Pipe, PipeTransform } from '@angular/core';
import { Person } from '../../core//interfaces//Person'
import * as _ from 'lodash';
@Pipe({
  name: 'sort'
})
///////////////////// IMPLEMENTATION OF SORTING //////////////////////////////////////////////////////////
export class SortPipe implements PipeTransform {
    transform(array: Person[],  sortByWhat?: string, ascOrDesc?: string): Person[] {

        if (!sortByWhat || !ascOrDesc) {
            return array; // return array if no sort parameters
        }
        else if(sortByWhat != "dateOfBirth"){ // the way of sorting by name/company/note
            return _.orderBy(array, sortByWhat, ascOrDesc)
        }
        else{ // the way of sorting by date
            return _.orderBy(array, function(el){
                let dateArray = el.dateOfBirth.split(" ")[0].split("."); // split date string and get date
                let time = el.dateOfBirth.split(" ")[1]; // split date string and get time
                if(dateArray[0].length == 1) dateArray[0] = "0" + dateArray[0]; // add 0 if one-digit
                if(dateArray[1].length == 1) dateArray[1] = "0" + dateArray[1]; // add 0 if one-digit
                return dateArray[2] + "." + dateArray[1] + "." + dateArray[0] + " " + time; // reverse date and return
            }, ascOrDesc)
        }
    }
};
import { Pipe, PipeTransform } from '@angular/core';
import { Person } from '../../core//interfaces//Person'
@Pipe({
  name: 'pagination'
})
///////////////////// IMPLEMENTATION OF PAGINATION ////////////////////////////////////////////////////
export class PaginationPipe implements PipeTransform {
    transform(array: Person[],  pageSize?: number, pageCount?: number): Person[] {

        if (!pageSize || !pageCount || !array) {
            return array; // return array if  no page size or page number
        }
        else {
            // Calculate index of first element
            let start = (array.length > (pageCount-1)*pageSize)? (pageCount-1)*pageSize:array.length-1;
             // Calculate index of last element
            let limit = (array.length > pageCount*pageSize) ? pageCount*pageSize:array.length;
            // Return appropriate slice
            return array.slice(start,limit);
        }
    }
};
import { NgModule } from '@angular/core'; 
import { TableModule } from './table/table.module'
import { MainComponent } from './main.component';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
    imports: [ TableModule ],
    declarations: [ MainComponent, NavbarComponent],
    exports: [ MainComponent ]
    })
export class MainModule {}
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { MockedApi } from '../../core/services/mockedapi.service';
import { SlicePipe } from '@angular/common';
import { Person } from '../../core/interfaces/Person';

@Component({
  selector: 'table',
  styleUrls: [ './table.component.sass'],
  templateUrl: './table.component.jade',
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'pl'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
})
export class TableComponent implements OnInit {

  // Variable Declarations 

  listOfPersons: Person[]; // list of employee's displayed in the table
  
  filterDate: string; // The date in string format that is passing to filter function

  sortedAscOrDesc: string; // The parameter of sorting ("asc" or "desc")

  sortedBy: string; // Sorting attribute 

  pageSize: number; // Page size in pagination (5)

  currentPage: number; // The number of current page that is passing to pagination pipe

  // Filter form with all necessery parameters
  private filterTable = this.fb.group({
    firstName: [''],
    lastName: [''],
    date: [''],
    company: [''],
    note: [''],
  });


  constructor(private _mockedApi: MockedApi, private fb: FormBuilder) {}

  ngOnInit() {
    this.pageSize = 5; // Initialization of page size
    this.currentPage = 1; // Initialization of page number
    this._mockedApi.getData() // Getting data from file
      .subscribe(data =>{
          this.listOfPersons = data;
    });
  }

  // Filtering function
  filterData(){
    const filterArg = this.filterTable.getRawValue(); // Get parameters from form

    // Pass parameters to service and get filtered data
    this.listOfPersons = this._mockedApi.filterData(filterArg.firstName, filterArg.lastName, 
                                          filterArg.company, this.filterDate, filterArg.note);

    /// Change page if necessery
    if(this.listOfPersons.length < this.currentPage*this.pageSize){
      this.currentPage = Math.max(Math.ceil(this.listOfPersons.length/this.pageSize),1);
    }
  }

  // Sorting function 
  sortElements(value){
    // Set sorting parameters to asc or desc
    if(value == this.sortedBy){
      this.sortedAscOrDesc = (this.sortedAscOrDesc=="desc") ? "asc":"desc";
    }
    else{
      // Get new attribute od sorting
      this.sortedBy = value;
      this.sortedAscOrDesc = "asc";
    }
  }

  // Incrementation of page number
  nextPage(){
    this.currentPage++;
  }

  // Decrementation of page number
  prevPage(){
    if(this.currentPage > 1)
      this.currentPage--;
  }

  // Getting new date in string format and calling filter function
  setDate(value){
    this.filterDate=value;
    this.filterData();
  }

}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';  
import { FormsModule , ReactiveFormsModule} from '@angular/forms';

import { MatButtonModule } from '@angular/material/button';
import { MatInputModule} from '@angular/material';
import { MatNativeDateModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { TableComponent } from './table.component'
import { SortPipe } from '../../shared/pipes/sort.pipe'
import { PaginationPipe } from '../../shared/pipes/pagination.pipe'
  
  @NgModule({
    imports: [  
      CommonModule, 
      FormsModule, 
      ReactiveFormsModule, 
      MatInputModule,
      MatNativeDateModule, 
      MatDatepickerModule,
      MatButtonModule,
    ],
    declarations: [ TableComponent, SortPipe, PaginationPipe ],
    exports: [ TableComponent ]
  })
  export class TableModule {}
  
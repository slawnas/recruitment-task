import { Component } from '@angular/core';

@Component({
  selector: 'navbar',
  styleUrls: [ './navbar.component.sass'],
  templateUrl: './navbar.component.jade',
})
export class NavbarComponent  {

  /// Implementation of component that is on the top of the screen (logo)

  constructor() {}

}
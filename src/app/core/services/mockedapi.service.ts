import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Person } from "app/core/interfaces/person";
import 'rxjs/add/operator/map';


@Injectable()
export class MockedApi {

    listOfPersons: Person[]; // list of employees

    constructor(private http: HttpClient){}

    getData(): Observable<Person[]> { // get data from json file
        return this.http.get<Person[]>('../../../assets/mock-data/data.json')
        .map(res => this.listOfPersons = res);
    }

///////////////////// IMPLEMENTATION OF FILTERING //////////////////////////////////////////////////////////

    filterData(firstName: string, lastName: string, company:string, date:string, note: string): Person[] {

        if (!firstName && !lastName  && !date && !note && !company) {
            return this.listOfPersons; // return array if there is no need of filtering
        }
        else{

            let result = this.listOfPersons; // copy of data

            if(firstName){
                result = this.filterByString(result, "firstName", firstName); // filter by firstname
            }

            if(lastName){
                result = this.filterByString(result,"lastName", lastName); // filter by lastname
            }

            if(date){
                result = this.filterByDate(result, date); // filter by date
            }

            if(company){
                result = this.filterByString(result, "company", company); // filter by company
            }

            if(note){
                result = this.filterByNumber(result,"note", note); // filter by note
            }

            return result; // return result
        }
    }

    // Filtering by firstname/lastname/company
    filterByString(array: Person[], param: string, value: string){
        return array.filter(
            element => {
                if (!element[param] || !param) return false;
                return element[param].toLowerCase().includes(value.toLowerCase()); // get filtered objects
            }
        )
    }

    // Filtering by note
    filterByNumber(array: Person[], param: string, value: string){
        return array.filter(
            element => {
                if (!element[param] || !param) return false;
                return element[param] == value; // get filtered objects
            }
        )
    }

    // Filtering by date
    filterByDate(array: Person[], date: string){
        return array.filter(
            element => {

                if (!element.dateOfBirth || !date) return false;
                
                let toString=element.dateOfBirth; // Date of birth from list
                let dateArray = date.split("."); // Splitting a date

                //  There are 3 possibilities  of date format: 
                // - DD.MM.YYYY e.g 01.01.2018, 10.12.1990
                // - D.MM.YYYY e.g 1.01.2018, 7.12.1990
                // - D.M.YYYY e.g 1.1.2018, 7.7.1990
                let DDMMYYYY , DMMYYYY , DMYYYY;

                // Creating all possibilities of date
                if(dateArray[0])
                {
                    if(dateArray[0].length == 1){ // if day is one-digit number
                        DMMYYYY = dateArray[0];
                        DMYYYY = dateArray[0];
                        DDMMYYYY = "0" + dateArray[0];
                    }
                    else{ // if day is two-digit number
                        DDMMYYYY = dateArray[0];
                        if(dateArray[0][0] == "0"){ // if it is 1 - 9
                            DMMYYYY = dateArray[0][1];
                            DMYYYY = dateArray[0][1];
                        }
                        else{ //if it is 10-31
                            DMYYYY = dateArray[0];
                            DMMYYYY = dateArray[0];
                        }     
                }}

                if(dateArray[1])
                {
                    if(dateArray[1].length == 1){  // if month is one-digit number
                        DMMYYYY = DMMYYYY + ".0" + dateArray[1] + ".";
                        DMYYYY =  DMYYYY + "." + dateArray[1] + ".";
                        DDMMYYYY = DDMMYYYY + ".0" + dateArray[1] + ".";
                    }
                    else{  // if month is two-digit number
                        
                        DMMYYYY = DMMYYYY + "." + dateArray[1] + ".";
                        DDMMYYYY = DDMMYYYY + "." + dateArray[1] + ".";

                        if(dateArray[1][0] == "0"){ // if it is january - september
                            DMYYYY = DMYYYY + "." + dateArray[1][1] + ".";
                        }
                        else{ //if it is october - december
                            DMYYYY = DMYYYY + "." + dateArray[1] + ".";
                        }
                    }
                }

                // adding a year
                if(dateArray[2]){
                    DMMYYYY += dateArray[2];
                    DDMMYYYY += dateArray[2];
                    DMYYYY += dateArray[2];
                }

                // get filtered objects (all possibilities)
                return  toString.toLowerCase().includes(DMMYYYY) ||
                        toString.toLowerCase().includes(DDMMYYYY) ||
                        toString.toLowerCase().includes(DMYYYY) ||
                        toString.toLowerCase().includes(date);
            }
        )
    }
}

export interface Person {
    "id": number,
    "firstName": string,
    "lastName": string,
    "dateOfBirth": string,
    "company": string,
    "note": number;
}
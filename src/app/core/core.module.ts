/* tslint:disable:member-ordering no-unused-variable */
import {ModuleWithProviders, NgModule, Optional, SkipSelf, ApplicationRef} from '@angular/core';
import { MockedApi } from './services/mockedapi.service';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  imports: [ HttpClientModule ],
  providers: [ MockedApi ]
})
export class CoreModule {

  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }
}

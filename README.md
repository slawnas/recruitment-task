# Recruitment Task

The implementation of recruitment task in Angular. There are employee’s personal data in data.json file.
The task was to present them in a table and to implement filtering (according to each attribute), sorting and pagination (5 results for one page).

---

## Start app

1. console: npm install
2. console: npm start
3. browser: localhost:4200

---

## File structure:  

1.	src/app/core - data services and interfaces:
    * /interfaces - interface for data from json file
    * /services - mockedApi service - implementation of filtering
2.	src/app/share/pipes:
    * pagination pipe - implementation of pagination
    * sort pipe - implementation of sorting
3.  src/app/main/navar - Navbar Component (jade, sass and ts files)
4.	src/app/main/table - Table Component (jade, sass and ts files):  
5.	src/app/assets - images and mock data
6.	src/app/styles - style config 

